﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp2
{
    class Program
    {

        static void Main(string[] args)
        {
            

        string create_grad = "CREATE TABLE IF NOT EXISTS grad (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "PostanskiBroj nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_pacijent = "CREATE TABLE IF NOT EXISTS pacijent (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Prezime nvarchar(50) not null," +
                "JMBG nvarchar(15) not null," +
                "Telefon nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "Grad_ID bigint not null," +
                "Pol nvarchar(10) not null," +
                "DatumRodjenja date not null, " +
                "FOREIGN KEY(Grad_ID) REFERENCES grad(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_poslovnaJedinica = "CREATE TABLE IF NOT EXISTS poslovnaJedinica (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Adresa nvarchar(50) not null," +
                "Telefon1 nvarchar(50) not null," +
                "Telefon2 nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_tipOsoblja = "CREATE TABLE IF NOT EXISTS tipOsoblja (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Zarada float null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_osoblje = "CREATE TABLE IF NOT EXISTS osoblje (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Prezime nvarchar(50) not null," +
                "Telefon nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "TipOsoblja_ID bigint not null," +
                "FOREIGN KEY(TipOsoblja_ID) REFERENCES tipOsoblja(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_odeljenje = "CREATE TABLE IF NOT EXISTS odeljenje (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "PoslovnaJedinica_ID bigint not null," +
                "FOREIGN KEY(PoslovnaJedinica_ID) REFERENCES poslovnaJedinica(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_izvestaj = "CREATE TABLE IF NOT EXISTS izvestaj (" +
                "ID bigint not null auto_increment," +
                "DatumKreiranja datetime not null," +
                "DatumZatvaranja datetime null," +
                "Anamneza nvarchar(2000) not null," +
                "Pacijent_ID bigint not null," +
                "FOREIGN KEY(Pacijent_ID) REFERENCES pacijent(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_akcija = "CREATE TABLE IF NOT EXISTS akcija (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Cena float not null," +
                "Trajanje int null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_istorijaIzvestaja = "CREATE TABLE IF NOT EXISTS istorijaIzvestaja (" +
                "ID bigint not null auto_increment," +
                "Datum datetime not null," +
                "Opis nvarchar(2000) not null," +
                "Cena float not null," +
                "Izvestaj_ID bigint not null," +
                "Akcija_ID bigint not null," +
                "Osoblje_ID bigint not null," +
                "SifraDijagnoze_ID bigint null," +
                "Ocena int null," +
                "FOREIGN KEY(Izvestaj_ID) REFERENCES izvestaj(ID)," +
                "FOREIGN KEY(Akcija_ID) REFERENCES Akcija(ID)," +
                "FOREIGN KEY(Osoblje_ID) REFERENCES osoblje(ID)," +
                "FOREIGN KEY(SifraDijagnoze_ID) REFERENCES SifraDijagnoze(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_dobavljac = "CREATE TABLE IF NOT EXISTS dobavljac (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Adresa nvarchar(150) not null," +
                "Email nvarchar(50) null," +
                "Telefon nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_opisLeka = "CREATE TABLE IF NOT EXISTS opisLeka (" +
                "ID bigint not null auto_increment," +
                "Tip nvarchar(50) not null," +
                "Naziv nvarchar(50) not null," +
                "Cena float null," +
                "Kolicina bigint not null," +
                "Opasan tinyint not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_lek = "CREATE TABLE IF NOT EXISTS lek (" +
                "ID bigint not null auto_increment," +
                "DatumUnosa datetime not null," +
                "NabavnaCena float not null," +
                "Cena float not null," +
                "OpisLeka_ID bigint not null," +
                "Dobavljac_ID bigint not null," +
                "RokTrajanja datetime not null," +
                "Kolicina int not null," +
                "FOREIGN KEY(OpisLeka_ID) REFERENCES OpisLeka(ID)," +
                "FOREIGN KEY(Dobavljac_ID) REFERENCES Dobavljac(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_odeljenjeOsoblje = "CREATE TABLE IF NOT EXISTS odeljenjeOsoblje(" +
                "ID bigint not null auto_increment," +
                "Odeljenje_ID bigint not null," +
                "Osoblje_ID bigint not null," +
                "FOREIGN KEY(Odeljenje_ID) REFERENCES Odeljenje(ID)," +
                "FOREIGN KEY(Osoblje_ID) REFERENCES Osoblje(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_sifraDijagnoze = "CREATE TABLE IF NOT EXISTS sifraDijagnoze(" +
                "ID bigint not null auto_increment," +
                "SifraDijagnoze nvarchar(50) not null," +
                "OpisDijagnoze nvarchar(1500) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_lek_istorijaIzvestaja = "CREATE TABLE IF NOT EXISTS lek_istorijaIzvestaja(" +
                "ID bigint not null auto_increment PRIMARY KEY," +
                "IstorijaIzvestaja_ID bigint not null," +
                "Lek_ID bigint not null," +
                "FOREIGN KEY(IstorijaIzvestaja_ID) REFERENCES IstorijaIzvestaja(ID)," +
                "FOREIGN KEY(Lek_ID) REFERENCES Lek(ID)" +
                ");";



            string createAllTables = create_grad +
                    create_pacijent + create_poslovnaJedinica +
                    create_tipOsoblja + create_osoblje + create_odeljenje +
                    create_izvestaj + create_akcija + create_sifraDijagnoze +
                    create_istorijaIzvestaja
                    + create_dobavljac +
                    create_opisLeka +
                    create_lek +
                    create_odeljenjeOsoblje +
                    create_lek_istorijaIzvestaja;

            runQuery(createAllTables);

            Console.WriteLine("Tabele su uspesno kreirane.");


            //inserti
            int akcijaCount = Int32.Parse(runQuery("SELECT count(id) from akcija"));
            int dobavljacCount = Int32.Parse(runQuery("SELECT count(id) from dobavljac"));
            int gradCount = Int32.Parse(runQuery("SELECT count(id) from grad"));
            int istorijaIzvestajaCount = Int32.Parse(runQuery("SELECT count(id) from istorijaizvestaja"));
            int izvestajCount = Int32.Parse(runQuery("SELECT count(id) from izvestaj"));
            int lekCount = Int32.Parse(runQuery("SELECT count(id) from lek"));
            int lekIstorijaIzvestajaCount = Int32.Parse(runQuery("SELECT count(id) from lek_istorijaizvestaja"));
            int odeljenjeCount = Int32.Parse(runQuery("SELECT count(id) from odeljenje"));
            int odeljenjeOsobljeCount = Int32.Parse(runQuery("SELECT count(id) from odeljenjeosoblje"));
            int opisLekaCount = Int32.Parse(runQuery("SELECT count(id) from opisleka"));
            int osobljeCount = Int32.Parse(runQuery("SELECT count(id) from osoblje"));
            int pacijentCount = Int32.Parse(runQuery("SELECT count(id) from pacijent"));
            int poslovnaJedinicaCount = Int32.Parse(runQuery("SELECT count(id) from poslovnajedinica"));
            int sifraDiijagnozeCount = Int32.Parse(runQuery("SELECT count(id) from sifradijagnoze"));
            int tipOsobljaCount = Int32.Parse(runQuery("SELECT count(id) from tiposoblja"));

            int sumCount = akcijaCount + dobavljacCount + gradCount + istorijaIzvestajaCount + izvestajCount + lekCount + lekIstorijaIzvestajaCount +
                            odeljenjeCount + odeljenjeOsobljeCount + opisLekaCount + osobljeCount + pacijentCount + poslovnaJedinicaCount +
                            sifraDiijagnozeCount + tipOsobljaCount;


            //inserti
            if (sumCount == 0)
            {
                createGradovi();
                createPacijenti();
                createPoslovneJedinice();
                createTipOsoblja();
                createOsoblje();
                createOdeljenje();
                createIzvestaji();
                createAkcija();
                createDobavljaci();
                createOpisLeka();
                createOdeljenjeOsoblje();
                createSifraDijagnoze();
                createIstorijaIzvetsaja();
                createLek();
                createLekIstorijaIzvestaja();
                Console.WriteLine("Uspesno su ispunjene tabele.");
            }

            






            string queries = "../queries.txt";
            string[] queriesList = File.ReadAllLines(queries);
            string query = "";

            while (true)

            {

                Console.WriteLine("\r\n\r\n1. Prikazati naziv akcije i broj izvodjenja po danu u nedelji");
                Console.WriteLine("2. Prikazati profit poliklinike po godinama");
                Console.WriteLine("3. Prikazi sve pacijente koji duze od jedne godine nisu radili analizu krvi");
                Console.WriteLine("4. Prikazi sve pacijente koji su bili vise od 5 puta");
                Console.WriteLine("5. Prikazati statistiku osoblja na osnovu ocena pacijenata");
                Console.WriteLine("6. Prikazati pacijente koji koriste rizicne lekove, lek i kolicinu u 2019. (u cilju da se otkriju potencijalni zavisnici)");
                Console.WriteLine("7. Poredjaj doktore po akcijama duzim od 30 min");
                Console.WriteLine("8. Poredjaj doktore po broju pacijenata za tekucu godinu");
                Console.WriteLine("9. Prikazati zaradu po doktoru za tekucu godinu");
                Console.WriteLine("10. Prikazati zaradu na lekovima za tekucu godinu");

                Console.WriteLine("11. Prikazati sve lekove na stanju");
                Console.WriteLine("12. Prikazati sve pacijente koji nisu iz Novog Sada a radili su pregled u poslovnoj jedinici u Novom Sadu");
                Console.WriteLine("13. Prikazati sve doktore koji su imali zakljucenu odredjenu dijagnozu");
                Console.WriteLine("14. Prikazi 5 pacijenata na kojima je poliklinika zaradila najvise novca");
                Console.WriteLine("15. Prikazati naziv leka na kom je najveca zarada");
                Console.WriteLine("16. Prikazati doktore koji nisu imali preglede duze od 1 mesec");
                Console.WriteLine("17. Poredjaj izvestaje koji nisu zatvoreni");
                Console.WriteLine("18. Poredjaj sestru koja je imala najvise istorije izvestaja");
                Console.WriteLine("19. Prikazati prosecan broj pregleda po danu");
                Console.WriteLine("20. Prikazati dobavljaca od kog je najveca zarada");

                Console.WriteLine("21. Prikazati lekara koji je izdao najvise lekova");
                Console.WriteLine("22. Prikazati 5 pacijenata koji su doneli najvise novca po izvestaju");
                Console.WriteLine("23. Prikazi 5 lekara koji su imali najvise pacijenata");
                Console.WriteLine("24. Prikazi sve lekove koji nisu utroseni a istekao im je rok");
                Console.WriteLine("25. Prikazati koje godine je bilo najvise pacijenata");
                Console.WriteLine("26. Prikazati koliko je prosecno izvestaja otvoreno prepodne");
                Console.WriteLine("27. Poredjaj koliko je prosecno akcija izvrseno prepodne");
                Console.WriteLine("28. Poredjaj koliko je prosecno vreme od otvaranja do zatvaranja izvestaja");
                Console.WriteLine("29. Prikazati sve zene koje duze od jedne godine nisu bile na ginekoloskom pregledu");
                Console.WriteLine("30. Prikazati sve muske pacijente starije od 50 godina i u poslednjih godinu dana nisu radili odredjeni pregled");
                Console.WriteLine("-------------------------------------------------------");
                Console.WriteLine("\r\nIzaberite upit koji zelite da se izvrsi: ");

                try
                {
                    int brojUpita = Int32.Parse(Console.ReadLine());
                    query = queriesList[brojUpita - 1];
                    Console.WriteLine(runQuery(query));
                }
                catch
                {
                    Console.WriteLine("Unesite validan broj");
                }



            }


            Console.ReadKey();

        }


        private static string runQuery(string text)
        {

            if (text == "")
            {
                return "";
            }

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=root;database=nova";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            MySqlCommand command = new MySqlCommand(text, databaseConnection);
            command.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                if (text.Split(' ')[0].ToLower().Equals("select"))
                {
                    MySqlDataReader myReader = command.ExecuteReader();


                    if (myReader.HasRows)
                    {

                        StringBuilder builder = new StringBuilder();
                        while (myReader.Read())
                        {

                            for (int i = 0; i < myReader.FieldCount; i++)
                            {
                                if (!myReader.IsDBNull(i))
                                {
                                    builder.Append(myReader.GetString(i));
                                }
                                else
                                {
                                    builder.Append("NULL");
                                }

                                if (i != myReader.FieldCount - 1)
                                {
                                    builder.Append("-");
                                }
                            }

                            builder.AppendLine();
                        } 
                        

                        return builder.ToString();
                    }
                    else
                    {
                        return "Query doesn't have result!";
                    }
                }
                else
                {
                    int numberOfRowsAffected = command.ExecuteNonQuery();
                    return numberOfRowsAffected + "has been affected!";
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "error";
            }
            finally
            {
                databaseConnection.Close();
            }
        }


        private static void createSifraDijagnoze()
        {

            Random random = new Random();
            string sifreDijagnoze = "";
            string querry = "";
            string readPath = "../";
            if (!File.Exists(readPath + "/sifraDijagnoze.txt"))
            {
                sifreDijagnoze += "INSERT INTO `sifradijagnoze`(`ID`, `SifraDijagnoze`, `OpisDijagnoze`) VALUES ";
                for (int i = 0; i < 900; i++)
                {
                    
                    int sifraDijagnoze = random.Next(1, 2000);
                    string opis = "Opis";
                    sifreDijagnoze += "(0, '" + sifraDijagnoze + "', '" + opis + "'),";
                }
                sifreDijagnoze = sifreDijagnoze.Substring(0, sifreDijagnoze.Length - 1);
                using (StreamWriter writeSifraDijagnoze = new StreamWriter(readPath + "/sifraDijagnoze.txt"))
                {

                    writeSifraDijagnoze.WriteLine(sifreDijagnoze);
                }

               
            }
            StreamReader objreader = new System.IO.StreamReader(readPath + "/sifraDijagnoze.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);

            Console.WriteLine("zavrsen sifrad");
        }

        private static void createLekIstorijaIzvestaja()
        {
            Random random = new Random();
            string insertLekoviIzvestaji = "";
            string querry = "";
            string readPath = "../";
            if (!File.Exists(readPath + "/lekIstorijaIzvestaja.txt"))
            {
                insertLekoviIzvestaji += "INSERT INTO `lek_istorijaizvestaja`(`ID`, `IstorijaIzvestaja_ID`, `Lek_ID`) VALUES ";
                for (int i = 0; i < 2000; i++)
                {
                 
                    int idLek = random.Next(1, 4000);
                    int idIstorijaIzvestaja = random.Next(1, 10000);
                    insertLekoviIzvestaji += "(0, " + idIstorijaIzvestaja + ", " + idLek + "),";
                }
                insertLekoviIzvestaji = insertLekoviIzvestaji.Substring(0, insertLekoviIzvestaji.Length - 1);
                using (StreamWriter writeLekIstorijaIzvestaja = new StreamWriter(readPath + "/lekIstorijaIzvestaja.txt"))
                {

                    writeLekIstorijaIzvestaja.WriteLine(insertLekoviIzvestaji);
                }
            }

            StreamReader objreader = new System.IO.StreamReader(readPath + "/lekIstorijaIzvestaja.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);

            Console.WriteLine("zavrsen lekist");
        }

        private static void createLek()
        {
            Random random = new Random();
            string insertLekovi = "";
            
            string readPath = "../";
            if (!File.Exists(readPath + "/lek.txt"))
            {
                insertLekovi += "INSERT INTO `lek`(`ID`, `DatumUnosa`, `NabavnaCena`, `Cena`, `OpisLeka_ID`, `Dobavljac_ID`, `RokTrajanja`, `Kolicina`) VALUES ";
                for (int i = 0; i < 4000; i++)
                {
                   
                    int range = 7 * 365; //7 years  
                    DateTime pastDate = DateTime.Today.AddDays(random.Next(range));
                    DateTime futureDate = DateTime.Today.AddDays(-random.Next(range));
                    pastDate = pastDate.AddHours(random.Next(0, 24));
                    futureDate = futureDate.AddHours(random.Next(0, 24));
                    string entryDate = Convert.ToDateTime(pastDate).ToString("yyyy-MM-dd HH:mm:ss");
                    string expirationDate = Convert.ToDateTime(futureDate).ToString("yyyy-MM-dd HH:mm:ss");
                    int idLeka = random.Next(1, 11);
                    int neededPrice = Int32.Parse(runQuery("select opisleka.cena from opisleka where ID =" + idLeka));
                    int idDobavljac = random.Next(1, 5);
                    double buyingPrice = neededPrice * 0.8;
                    float finalBuyingPrice = (float)buyingPrice;
                    int randomQuantity = random.Next(20, 400);
                    insertLekovi += "(0, '" + entryDate + "', " + finalBuyingPrice + ", " + neededPrice + ", " + idLeka + ", " + idDobavljac + ", '" + expirationDate + "', " + randomQuantity + "),";
                }
                insertLekovi = insertLekovi.Substring(0, insertLekovi.Length - 1);
                using (StreamWriter writeLek = new StreamWriter(readPath + "/lek.txt"))
                {

                    writeLek.WriteLine(insertLekovi );
                }
            }
            StreamReader objreader = new System.IO.StreamReader(readPath + "/lek.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);

            Console.WriteLine("zavrsen lek");

        }
        private static void createIstorijaIzvetsaja()
        {
            Random random = new Random();
            int idAkcija = 1;
            string insertIzvestaji = "INSERT INTO `istorijaizvestaja`(`ID`, `Datum`, `Opis`, `Cena`, `Izvestaj_ID`, `Akcija_ID`, `Osoblje_ID`, `SifraDijagnoze_ID`, `Ocena`) VALUES  ";
            
            string readPath = "../";
            if (!File.Exists(readPath + "/istorijaIzvestaja.txt"))
            {
                for (int i = 0; i < 10000; i++)
                {
                  
                    int idOsoblja = random.Next(1, 17);
                    int idIzvestaj = random.Next(1, 3000);
                    int randomOcena = random.Next(1, 11);
                    int randomHour = random.Next(0, 24);
                    int idSifraDijagnoze = random.Next(1, 900);

                    if (idOsoblja == 1 || idOsoblja == 10 || idOsoblja == 17)
                    {
                        idAkcija = 2;
                    } //idAkcija definisana ovde
                    else if (idOsoblja >= 2 && idOsoblja <= 4)
                    {
                        idAkcija = 1;
                    }
                    else if (idOsoblja == 5 || idOsoblja == 6)
                    {
                        idAkcija = 8;
                    }
                    else if ((idOsoblja > 6 && idOsoblja < 10) || idOsoblja == 13 || idOsoblja == 14)
                    {
                        idAkcija = 6;
                    }
                    else if (idOsoblja == 11)
                    {
                        idAkcija = 9;
                    }
                    else if (idOsoblja == 12)
                    {
                        idAkcija = 10;
                    }
                    else if (idOsoblja == 15)
                    {
                        idAkcija = 4;
                    }
                    else if (idOsoblja == 16)
                    {
                        idAkcija = 3;
                    }

                    string neededDate = runQuery("SELECT LEFT(izvestaj.DatumKreiranja , 10) from izvestaj where ID=" + idIzvestaj);
                  
                    neededDate += " " + randomHour + ":00:00";
                    int price = Int32.Parse(runQuery("SELECT akcija.Cena from akcija WHERE id=" + idAkcija));
                    double price1 = (float)price;
                    string desc = "Opis";

                    if (idSifraDijagnoze < 60)
                    {
                      
                            insertIzvestaji += "(0,'" + neededDate + "','" + desc + "'," + price1 + "," + idIzvestaj + "," + idAkcija + "," + idOsoblja + ",null," + randomOcena + "),";  
                    }
                    else
                    {                     
                            insertIzvestaji += "(0,'" + neededDate + "','" + desc + "'," + price1 + "," + idIzvestaj + "," + idAkcija + "," + idOsoblja + "," + idSifraDijagnoze + "," + randomOcena + "),";
                        
                    }

                }
                insertIzvestaji = insertIzvestaji.Substring(0, insertIzvestaji.Length - 1);
                using (StreamWriter writeIstorijaIzvestaja = new StreamWriter(readPath + "/istorijaIzvestaja.txt"))
                {

                    writeIstorijaIzvestaja.WriteLine(insertIzvestaji);
                }

            }
            StreamReader objreader = new System.IO.StreamReader(readPath + "/istorijaIzvestaja.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);


            Console.WriteLine("zavrsen istizv");
        }

        private static void createOdeljenjeOsoblje()
        {
            string insertQueries = "INSERT INTO `odeljenjeosoblje`(`ID`, `Odeljenje_ID`, `Osoblje_ID`) VALUES " +
                "(0,1,1), (0,1,2), (0,2,13), (0,2,3), (0,2,14), (0,3,8), (0,3,4), " +
                "(0,4,9), (0,5,7), (0,5,6), (0,6,11), (0,7,12), (0,8,10), (0,9,9), (0,10,15), (0,11,17), (0,11,5)";
                string result = runQuery(insertQueries);
        }

        private static void createOpisLeka()
        {
            string insertQueries = "INSERT INTO opisleka(ID, Tip, Naziv, Cena, Kolicina, Opasan) VALUES" +
                "(0,'Antipiretik','Aspirin',300,400,1), (0,'Fizioloski rastvor','NaCl',200,1000,0), (0,'Lek za smirenje','Bromazepam',500,500,1), " +
                "(0,'Antiseptik','Rivanol',180,200,0), (0,'Antibiotik','Azitromicin',1200,400,0), (0,'Butamirat','Sinetus',900,200,0), " +
                "(0,'Antidijabetik','Glucophage',600,100,0), (0,'Nepoznat','Eftil',100,400,1), (0,'H2-Blokator','Famotidin Alkaloid',300,400,1)," +
                "(0,'Nepoznat','Atacor',300,400,0), (0,'Analgetik','Novalgetol',90,900,0);";

            string result = runQuery(insertQueries);
              
        }

        private static void createDobavljaci()
        {
            string insertQueries = "INSERT INTO dobavljac(ID, Ime, Adresa, Email, Telefon) VALUES " +
               "(0,'Herbalife','Sinise Mihajlovica 4 - Beograd','herbalife@gmail.com','06561123379'), (0,'MedicoSr','Isidore Bjelice 2 - Beograd','medicosr@gmail.com','06321123379'), " +
               "(0,'Medic','Nikole Tesle 11 - Novi Sad','medic@gmail.com','06111123379'), (0,'Lekovi-Srbije','Branislava Nusica 19 - Nis','lekovisrbije@gmail.com','06561444379'), " +
               "(0,'naturalherb','Karadjordjeva 11 - Leskovac','naturalherb@gmail.com','06561123322')";


            string result = runQuery(insertQueries);
               
        }

        private static void createAkcija()
        {
            string insertQueries = "INSERT INTO akcija(ID, Naziv, Cena, Trajanje) VALUES " +
                "(0,'Vadjenje krvi',300,5), (0,'Opsti pregled',2000,20), (0,'Internisticki pregled',3000,30), (0,'Kardioloski pregled',3000,45)," +
                "(0,'Hirurski pregled',3000,20), (0,'Hirurska intervencija',10000,120), (0,'Plasticna hirurgija',20000,160), (0,'Bris grla',300,2), " +
                "(0,'Ginekoloski pregled',3000,20), (0,'Uroloski pregled',3000,20)";


            string result = runQuery(insertQueries);
                
        }

        
        private static void createIzvestaji()
        {
            

            Random random = new Random();
            string insertIzvestaji = "INSERT INTO `izvestaj`(`ID`, `DatumKreiranja`, `DatumZatvaranja`, `Anamneza`, `Pacijent_ID`) VALUES ";


            string readPath = "../";
            if (!File.Exists(readPath + "/izvestaj.txt"))
            {
                for (int i = 0; i < 3000; i++)
                {
                 
                    int range1 = 9 * 365; //7 years  
                    int range2 = 5 * 365;
                    int range3 = 2 % 365; 
                    DateTime pastDate = DateTime.Today.AddDays(-random.Next(range2,range1));
                    
                    DateTime futureDate = DateTime.Today.AddDays(-random.Next(range3,range2));
                    futureDate = futureDate.AddHours(random.Next(0, 24));
                    pastDate = pastDate.AddHours(random.Next(0, 24));
                    string datum1 =Convert.ToDateTime(pastDate).ToString("yyyy-MM-dd");
                    string datum2 =Convert.ToDateTime(futureDate).ToString("yyyy-MM-dd");
                    int randsat = random.Next(0, 24);
                    datum1 += " " + randsat + ":00:00";
                    int randsat2 = random.Next(0, 24);
                    datum2 += " " + randsat2 + ":00:00";
                    string anamneza = "Anamneza";
                    int pacijentID = random.Next(1, 10000);

                    if (randsat > 16)
                    {
                       
                            insertIzvestaji += "(0,'" + datum1 + "',null,'" + anamneza + "'," + pacijentID + "),";
                    }
                    else
                        {
                            insertIzvestaji += "(0,'" + datum1 + "','" + datum2 + "','" + anamneza + "'," + pacijentID + "),";
                        }
                    
                }
                insertIzvestaji = insertIzvestaji.Substring(0, insertIzvestaji.Length - 1);
                using (StreamWriter writeIzvestaj = new StreamWriter(readPath + "/izvestaj.txt"))
                {

                    writeIzvestaj.WriteLine(insertIzvestaji);
                }
            }
            
            
            StreamReader objreader = new System.IO.StreamReader(readPath + "/izvestaj.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);



            Console.WriteLine("zavrsen izvestaji");
        }

        private static void createOdeljenje()
        {
            string insertQueries = "INSERT INTO odeljenje(ID, Naziv, PoslovnaJedinica_ID) VALUES " +
                "(0,'Opsta Praksa',1), (0,'Hirurgija',3), (0,'Hitna Sluzba',3), (0,'Hitna Sluzba',1), (0,'Prijemna Ambulanta',1), (0,'Ginekologija',2), " +
                "(0,'Urologija',2), (0,'Opsta Praksa',2), (0,'Internisticko Odeljenje',1), (0,'Kardiologija',1), (0,'Medicina Rada',3);";

            string result = runQuery(insertQueries);
                
        }
        private static void createOsoblje()
        {
            string insertQueries = "INSERT INTO osoblje(ID, Ime, Prezime, Telefon, Email, TipOsoblja_ID) VALUES " +
                "(0,'Marko','Stanojevic','0645557711','markostanojevic@gmail.com',1), (0,'Aleksandra','Gombos','0645337711','aleksandragombos@gmail.com',2)," +
                "(0,'Tina','Stojilkovic','0645557722','tinastojilkovic@gmail.com',2), (0,'Valentina','Mitic','0635557711','valentinamitic@gmail.com',2), " +
                "(0,'Natasa','Jotic','06453327711','natasajotic@gmail.com',2), (0,'Jelena','Stanisic','0645544411','jelenastanisic@gmail.com',2)," +
                "(0,'Nina','Buha','0621157711','ninabuha@gmail.com',3), (0,'Nikolina','Djordjevic','0651557711','nikolinadjordjevic@gmail.com',3)," +
                "(0,'Andjela','Kovacevic','064332111','andjelakovacevic@gmail.com',3), (0,'Andrea','Biskupovic','0648897711','andreabiskupovic@gmail.com',1), " +
                "(0,'Nemanja','Spasic','0645557700','nemanjaspasic@gmail.com',4), (0,'Marko','Zahorodni','0645332711','markozahorodni@gmail.com',5)," +
                "(0,'Mina','Gusman','0645550001','minagus@gmail.com',6), (0,'Bogdan','Teofanovic','0615551711','bogteofanovic@gmail.com',7)," +
                "(0,'Stefan','Komarica','0699957711','stefankom@gmail.com',8), (0,'Milos','Komarica','0639002133','miloskomarica@gmail.com',9), " +
                "(0,'Biljana','Dobric','0635254711','biljanadobric@gmail.com',1)";
            string result = runQuery(insertQueries);
               
        }

        private static void createTipOsoblja()
        {
            string insertQueries = "INSERT INTO tiposoblja(ID, Naziv, Zarada) VALUES " +
                "(0,'Doktor Opste Prakse',90000), (0,'Medicinska Sestra',40000), (0,'Glavna Medicinska Sestra',50000), (0,'Ginekolog',100000), " +
                "(0,'Urolog',100000), (0,'Hirurg',120000), (0,'Plasticni Hirurg',150000), (0,'Kardiolog',110000), (0,'Internista',110000)";


            string result = runQuery(insertQueries);
               
        }

        private static void createPoslovneJedinice()
        {
            string insertQueries = "INSERT INTO poslovnajedinica(ID, Naziv, Adresa, Telefon1, Telefon2, Email) VALUES " +
                "(0,'Med+ Grbavica','Mise Dimitrijevica 3, Novi Sad','0653398800','064111222334','medplusgrbavica@gmail.com'), " +
                "(0,'Med+ Centar','Zmaj Jovina 33, Beograd','0653398801','064111222335','medpluscentar@gmail.com'), " +
                "(0,'Med+ Podbara','Laze Kostica 43, Novi Sad','0653398802','064111222336','medpluspodbara@gmail.com');";

            string result = runQuery(insertQueries);
              
        }

        private static void createGradovi()
        {
            string insertQueries = "INSERT INTO grad(ID, Naziv, PostanskiBroj) VALUES " +
                "(0,'Kovin','26220'), (0,'Novi Sad','21000'), (0,'Beograd','11000'), (0,'Krusevac','37000'), (0,'Novi Pazar','36300'), " +
                "(0,'Knjazevac','19350'), (0,'Nis','18000'), (0,'Paracin','35250'), (0,'Leskovac','16000'), (0,'Smederevo','11300'), " +
                "(0,'Pancevo','26000'), (0,'Valjevo','14000'), (0,'Nova Pazova','22330'), (0,'Stara Pazova','22300'), " +
                "(0,'Indjija','22320'), (0,'Uzice','31000'), (0,'Kraljevo','36000'), (0,'Subotica','24000'), (0,'Cacak','32000');";


            string result = runQuery(insertQueries);
               
        }

        private static void createPacijenti()
        {
            string firstNames = "../random-name/first-names.txt";
            string[] firstNamesList = File.ReadAllLines(firstNames);
            string names = "../random-name/names.txt";
            string[] namesList = File.ReadAllLines(names);
            string insertPacijenti = "";
     
            Random random = new Random();

            string readPath = "../";
            if (!File.Exists(readPath + "/pacijent.txt"))
            {
                for (int i = 0; i < 10000; i++)
                {
                   
                    int randomFirstNameNumber = random.Next(4000);
                    int randomNameNumber = random.Next(20000);

                    String ime = firstNamesList[randomFirstNameNumber];
                    String prezime = namesList[randomNameNumber];
                    String jmbg = random.Next(100000, 999999) + "" + random.Next(1000000, 9999999);
                    String telefon = "06" + random.Next(10000000, 99999999);
                    String email = ime.ToLower() + prezime.ToLower() + "@gmail.com";
                    String pol = "muski";

                    int birthDateYear = random.Next(1900, 2019);
                    int birthDateMonth = random.Next(1, 12);
                    int birthDateDay = random.Next(1, DateTime.DaysInMonth(birthDateYear, birthDateMonth));

                    int randomDay = birthDateDay + random.Next(1, 28);
                    int randomMonth = birthDateMonth + random.Next(1, 11);
                    int randomYear = birthDateYear + random.Next(1, 3);
                    string birthDate = birthDateYear + "-" + birthDateMonth + "-" + birthDateDay;

                    if (random.Next(10) % 2 == 0)
                    {
                        pol = "zenski";
                    }

                    insertPacijenti += "(0,'" + ime + "','" + prezime + "','" + jmbg + "','" + telefon + "','" + email + "'," + random.Next(1, 20) + ",'" + pol + "','" + birthDate + "'),";

                }
                insertPacijenti = "insert into `pacijent`(`ID`, `Ime`, `Prezime`, `JMBG`, `Telefon`, `Email`, `Grad_ID`, `Pol`, `DatumRodjenja`) values " + insertPacijenti;
                insertPacijenti = insertPacijenti.Substring(0, insertPacijenti.Length - 1);
                using (StreamWriter writePacijent = new StreamWriter(readPath + "/pacijent.txt"))
                {

                    writePacijent.WriteLine(insertPacijenti);
                }
            }
            
            StreamReader objreader = new System.IO.StreamReader(readPath + "/pacijent.txt");
            string createQuery = objreader.ReadToEnd();
            objreader.Close();
            string result = runQuery(createQuery);

            Console.WriteLine("zavrsen pacijenti");

        }

    }
}
